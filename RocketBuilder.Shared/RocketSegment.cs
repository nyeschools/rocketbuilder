﻿using Core.Shared;
using Core.Shared.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Linq;

namespace RocketBuilder.Shared
{
    [Entity("RocketSegment")]
    public class RocketSegment : Entity
    {
        public RocketNode TopNode { get; private set; }

        public RocketNode BotNode { get; private set; }

        public RocketSegment(Package package) : base(package)
        {
            TopNode = Entities.OfType<RocketNode>().First(s => s.Name.StartsWith("Top"));
            BotNode = Entities.OfType<RocketNode>().First(s => s.Name.StartsWith("Bot"));
        }

        public void SetTexture(Texture2D texture)
        {
            GetComponent<DrawComponent>().Texture = texture;

            //Top node, set to top of texture height
            var tt = TopNode.GetComponent<TransformComponent>();
            tt.Parent = GetComponent<TransformComponent>();
            tt.Position = new Vector2(0, -texture.Height);

            //Bottom node, already at bottom
            var bt = BotNode.GetComponent<TransformComponent>();
            bt.Parent = GetComponent<TransformComponent>();
            bt.Position = new Vector2(0, 0);
        }
    }
}
