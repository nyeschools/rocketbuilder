﻿using Core.Shared;
using Core.Shared.Components;
using Core.Shared.Glide;
using Definitions.Camera;
using Definitions.Input;
using Definitions.Input.Callbacks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RocketBuilder.Shared.Components;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Systems.Shared.Graphics;
using Systems.Shared.Update;

namespace RocketBuilder.Shared
{
    [Entity("RocketBuilder")]
    public class RocketBuilder : Entity, IMouseCallback
    {
        private Int32 _SegementIndex = 0;
        private String _SegmentName => _RocketSegments.Keys.ToList()[_SegementIndex];
        private Int32 _Index;
        private RocketSegment Segement => _RocketSegments[_SegmentName][_Index];

        private readonly String _RocketRootFolder;
        private readonly String[] _RocketFolders;

        private readonly String _RocketSpriteSource;

        private readonly Dictionary<String, List<RocketSegment>> _RocketSegments = new Dictionary<String, List<RocketSegment>>();

        private Tweener _Tweener;
        private Boolean _Moving;

        public RocketBuilder(Package package) : base(package)
        {
            _RocketRootFolder = GetComponent<RocketRootFolderComponent>().Value;
            _RocketFolders = GetComponent<RocketFoldersComponent>().Value;

            _RocketSpriteSource = GetComponent<RocketSegmentSourceComponent>().Value;

            foreach (var rocketFolder in _RocketFolders)
            {
                _RocketSegments.Add(rocketFolder, new List<RocketSegment>());
            }
        }

        public override void Start()
        {
            base.Start();

            _Tweener = new Tweener();
            World.GetSystem<IUpdateSystem>().Register(_Tweener);
            World.GetSystem<IInputSystem>().Register(this);

            foreach (var rocketFolder in _RocketFolders)
            {
                var camera = World.GetSystem<ICamera>();
                var cameraSize = camera.Bounds.Size;

                var content = World.GetSystem<IContentSystem>().Content;

                var folder = Path.Combine(content.RootDirectory, $"{_RocketRootFolder}/{rocketFolder}");
                var examples = Directory.GetFiles(folder);

                foreach (var example in examples)
                {
                    var n = example.TrimStart($"{content.RootDirectory}\\".ToCharArray());
                    n = n.TrimEnd(".xnb".ToCharArray());
                    var t = content.Load<Texture2D>(n);

                    RocketSegment rocketSegment = World.Create<RocketSegment>(_RocketSpriteSource);
                    rocketSegment.SetTexture(t);

                    //Position to bottom of screen
                    rocketSegment.GetComponent<TransformComponent>().Position = new Vector2(cameraSize.X / 2f, cameraSize.Y);

                    _RocketSegments[rocketFolder].Add(rocketSegment);
                    Attach(rocketSegment);
                }
            }

            foreach (var rocketSegment in _RocketSegments.SelectMany(s => s.Value)) rocketSegment.Visible = false;
            Segement.Visible = true;
        }

        public void MousePressed(Vector2 mousePosition)
        {
            if (_Moving) return;
            var zone = 200;
            var camera = World.GetSystem<ICamera>();
            var size = camera.Bounds.Size;

            if (mousePosition.X > 0 && mousePosition.X < zone)
            {
                SwipeLeft();
            }
            else if (mousePosition.X > size.X - zone && mousePosition.X < size.X)
            {
                SwipeRight();
            }
            //Center screen?
            else if (Math.Abs(camera.TransformComponent.AbsolutePosition.X + camera.Bounds.Width / 2f - mousePosition.X) < 200)
            {
                if (mousePosition.Y < camera.Bounds.Height / 2f)
                {
                    NextSegement();
                }
                else
                {
                    PreviousSegement();
                }
            }
        }

        private void SwipeLeft()
        {
            _Moving = true;

            var moveTime = 0.2f;

            var camera = World.GetSystem<ICamera>();

            var currentSegment = Segement;
            var currentBotNodeTrans = currentSegment.BotNode.GetComponent<TransformComponent>();
            var currentBotNodePos = currentBotNodeTrans.AbsolutePosition;

            var d = 100f;

            //Hide all the segements
            for (var i = 0; i < _RocketSegments[_SegmentName].Count; i++)
            {
                var rocketSegment = _RocketSegments[_SegmentName][i];
                rocketSegment.Visible = false;
            }

            currentSegment.Visible = true;

            //Move the currentSegement to the left of the screen
            _Tweener.Tween(currentSegment.GetComponent<TransformComponent>(),
                new { Position = new Vector2(-d, currentBotNodePos.Y) }, moveTime);

            //change move to the next segement
            _Index = (_Index + 1) % _RocketSegments[_SegmentName].Count;

            var next = _RocketSegments[_SegmentName][_Index];
            var nextLeftTransform = next.GetComponent<TransformComponent>();
            nextLeftTransform.Position = new Vector2(camera.Bounds.Width + d, currentBotNodePos.Y);

            next.Visible = true;

            var endPos = new Vector2(camera.Bounds.Width / 2f, currentBotNodePos.Y);
            _Tweener.Tween(nextLeftTransform, new { Position = endPos }, moveTime)
                .OnComplete(() =>
                {
                    for (var i = 0; i < _RocketSegments[_SegmentName].Count; i++)
                    {
                        var rocketSegment = _RocketSegments[_SegmentName][i];
                        rocketSegment.Visible = i == _Index;
                    }

                    _Moving = false;
                });
        }

        private void SwipeRight()
        {
            _Moving = true;

            var moveTime = 0.2f;

            var camera = World.GetSystem<ICamera>();

            var currentSegment = Segement;
            var currentBotNodeTrans = currentSegment.BotNode.GetComponent<TransformComponent>();
            var currentBotNodePos = currentBotNodeTrans.AbsolutePosition;

            var d = 100f;

            //Hide all the segements
            for (var i = 0; i < _RocketSegments[_SegmentName].Count; i++)
            {
                var rocketSegment = _RocketSegments[_SegmentName][i];
                rocketSegment.Visible = false;
            }

            currentSegment.Visible = true;

            //Move the currentSegement to the right of the screen
            _Tweener.Tween(currentSegment.GetComponent<TransformComponent>(),
                new { Position = new Vector2(camera.Bounds.Width + d, currentBotNodePos.Y) }, moveTime);

            //change move to the next segement
            _Index = (_Index + 1) % _RocketSegments[_SegmentName].Count;

            var next = _RocketSegments[_SegmentName][_Index];
            var nextLeftTransform = next.GetComponent<TransformComponent>();
            nextLeftTransform.Position = new Vector2(-d, currentBotNodePos.Y);

            next.Visible = true;

            var endPos = new Vector2(camera.Bounds.Width / 2f, currentBotNodePos.Y);
            _Tweener.Tween(nextLeftTransform, new { Position = endPos }, moveTime)
                .OnComplete(() =>
                {
                    for (var i = 0; i < _RocketSegments[_SegmentName].Count; i++)
                    {
                        var rocketSegment = _RocketSegments[_SegmentName][i];
                        rocketSegment.Visible = i == _Index;
                    }

                    _Moving = false;
                });
        }

        private void NextSegement()
        {
            if (_SegementIndex >= _RocketSegments.Count - 1) return;

            var node = Segement.GetChild<RocketNode>(s => s.Name.StartsWith("Top"));
            var topNodePos = node.GetComponent<TransformComponent>().AbsolutePosition;

            _Index = 0;
            _SegementIndex++;

            for (var i = 0; i < _RocketSegments[_SegmentName].Count; i++)
            {
                var rocketSegment = _RocketSegments[_SegmentName][i];
                var st = rocketSegment.GetComponent<TransformComponent>();
                st.Position = new Vector2(topNodePos.X, topNodePos.Y);

                rocketSegment.Visible = i == _Index;
            }
        }

        private void PreviousSegement()
        {
            if (_SegementIndex <= 0) return;

            //Hide all previous segments
            foreach (var rocketSegment in _RocketSegments[_SegmentName]) rocketSegment.Visible = false;

            _Index = 0;
            _SegementIndex--;

            var node = Segement.GetChild<RocketNode>(s => s.Name.StartsWith("Bot"));
            var botNodePos = node.GetComponent<TransformComponent>().AbsolutePosition;

            for (var i = 0; i < _RocketSegments[_SegmentName].Count; i++)
            {
                var rocketSegment = _RocketSegments[_SegmentName][i];
                var st = rocketSegment.GetComponent<TransformComponent>();
                st.Position = new Vector2(st.Position.X, botNodePos.Y);
            }
        }

        public void MouseReleased(Vector2 mousePosition)
        {

        }
    }
}
