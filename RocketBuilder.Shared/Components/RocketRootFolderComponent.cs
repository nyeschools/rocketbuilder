﻿using Core.Shared;
using Core.Shared.Components;
using System;
using System.Collections.Generic;

namespace RocketBuilder.Shared.Components
{
    [Component("RocketRootFolder")]
    public class RocketRootFolderComponent : SimpleComponent<String>
    {
        public RocketRootFolderComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {

        }
    }
}
