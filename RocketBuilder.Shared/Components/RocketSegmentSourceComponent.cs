﻿using Core.Shared;
using Core.Shared.Components;
using System;
using System.Collections.Generic;

namespace RocketBuilder.Shared.Components
{
    [Component("RocketSegmentSource")]
    public class RocketSegmentSourceComponent : SimpleComponent<String>
    {
        public RocketSegmentSourceComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {
        }
    }
}
