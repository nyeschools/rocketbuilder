﻿using Core.Shared;
using Core.Shared.Components;
using System;
using System.Collections.Generic;

namespace RocketBuilder.Shared.Components
{
    [Component("RocketFolders")]
    public class RocketFoldersComponent : ArrayComponent<String>
    {
        public RocketFoldersComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {

        }
    }
}
