﻿using Core.Shared;

namespace RocketBuilder.Shared
{
    [Entity("RocketNode")]
    public class RocketNode : Entity
    {
        public RocketNode(Package package) : base(package)
        {

        }
    }
}
